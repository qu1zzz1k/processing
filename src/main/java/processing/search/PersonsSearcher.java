package processing.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processing.domain.Person;
import processing.filter.Filter;
import processing.filter.FilterImpl;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static processing.converter.LinePersonConverter.fromLineToPerson;

public final class PersonsSearcher {
    private static Logger logger = LoggerFactory.getLogger(PersonsSearcher.class);

    private final String path;
    private final Filter filter;
    private final Comparator<Person> comparator;

    public PersonsSearcher(final Comparator<Person> comparator, final String path) {
        this.path = path;
        this.comparator = comparator;
        this.filter = new FilterImpl(comparator);
    }

    public Collection<Person> search(final int limit, final Map<Integer, Long> hashIndexMap) {
        logger.info("Started searching for " + limit + " unique rows based on comparator criteria...");
        Person[] sorted;

        try (final RandomAccessFile reader = new RandomAccessFile(path, "r")) {

            final Set<Integer> keySet = hashIndexMap.keySet();

            final Collection<Person> persons = keySet.stream().limit(limit).map(k -> {
                final long pointer = hashIndexMap.get(k);
                try {
                    reader.seek(pointer);
                    return fromLineToPerson(reader.readLine());
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    return null;
                }
            })
                    .collect(toList());

            sorted = (filter.filter(persons)).toArray(new Person[persons.size()]);

            if (keySet.size() <= limit) {
                logger.info("Unique rows count is less than " + limit + " returning sorted...");
                return Arrays.stream(sorted).collect(toList());
            }

            final Iterator<Integer> iterator = keySet.iterator();
            for (int i = 0; i < limit; i++) {
                iterator.next();
            }

            while (iterator.hasNext()) {
                int hash = iterator.next();
                final long pointer = hashIndexMap.get(hash);
                reader.seek(pointer);
                final String line = reader.readLine();
                final Person nextPerson = fromLineToPerson(line);

                int insertPos = limit;
                for (int i = sorted.length - 1; i >= 0; i--) {
                    if (comparator.compare(nextPerson, sorted[i]) < 0) {
                        insertPos = i;
                    } else break;
                }

                if (insertPos == limit - 1) sorted[insertPos] = nextPerson;
                else if (insertPos < limit) {
                    System.arraycopy(sorted, insertPos, sorted, insertPos + 1, limit - 1 - insertPos);
                    sorted[insertPos] = nextPerson;
                }
            }
        } catch (Exception e) {
            logger.error("Error during reading file. " + e.getLocalizedMessage());
            return emptyList();
        }

        logger.info("Search finished.");
        return Arrays.stream(sorted).collect(toList());
    }
}
