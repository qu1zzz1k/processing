package processing.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processing.domain.Person;
import processing.reader.UniqueRowsReader;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import static java.util.Collections.emptyList;
import static processing.converter.LinePersonConverter.fromLineToPerson;

public final class FileOrderPersonsSearcher {
    private static Logger logger = LoggerFactory.getLogger(UniqueRowsReader.class);
    private final String path;
    private final Collection<Person> persons;
    private final int limit;

    public FileOrderPersonsSearcher(final String path, final int limit){
        persons = new ArrayList<>(limit);
        this.limit = limit;
        this.path = path;
    }

    public Collection<Person> search(final Set<Integer> hashSet) {
        logger.info("Start searching persons in a file order...");
        try {
            try(final RandomAccessFile randomAccessFile = new RandomAccessFile(path, "r")) {
                while (persons.size() < limit) {
                    final String line = randomAccessFile.readLine();
                    if (hashSet.contains(line.hashCode()))
                        persons.add(fromLineToPerson(line));
                }

                return persons;
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            return emptyList();
        }
    }
}
