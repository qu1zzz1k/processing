package processing;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processing.domain.Person;
import processing.reader.UniqueRowsReader;
import processing.search.FileOrderPersonsSearcher;
import processing.search.PersonsSearcher;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args)  {
        final String filePath = Main.class.getClassLoader().getResource("input.txt").getPath();

        final UniqueRowsReader rowsReader = new UniqueRowsReader();
        rowsReader.read(filePath);
        final Map<Integer, Long> hashPositionsMap = rowsReader.getHashPointerMap();

        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        //Вывести первых 10 уникальных пользователей, отсортированных по возрасту, имени в порядке возрастания.
        final Comparator<Person> ageNameComparator = Comparator.comparing(Person::getAge)
                .thenComparing(Person::getName);
        final PersonsSearcher ps = new PersonsSearcher(ageNameComparator, filePath);
        final Collection<Person> persons = ps.search(10, hashPositionsMap);
        logger.info("10 unique persons in age/name order.");
        persons.forEach(p->logger.info(p.toString()));
        logger.info("Took - " +stopWatch);

        stopWatch.reset();
        stopWatch.start();

        //Вывести первых 10 уникальных пользователей, в исходном порядке (в порядке появления в файле).
        final FileOrderPersonsSearcher searcher = new FileOrderPersonsSearcher(filePath, 10);
        searcher.search(hashPositionsMap.keySet()).forEach(p->logger.info(p.toString()));
        logger.info("Took - " +stopWatch);
    }
}
