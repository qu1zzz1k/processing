package processing.converter;

import processing.domain.Person;

public final class LinePersonConverter {

    private LinePersonConverter(){}

    public static Person fromLineToPerson(final String line) {
        int mailEndPos = line.indexOf(":");
        int nameStartPos = mailEndPos + 2;
        int agePos = line.lastIndexOf(":") + 1;
        int nameEndPos = agePos - 2;

        return new Person(
                line.substring(0, mailEndPos),
                line.substring(nameStartPos, nameEndPos),
                Integer.parseInt(line.substring(agePos, line.length())));
    }
}
