package processing.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

public final class UniqueRowsReader {
    private static Logger logger = LoggerFactory.getLogger(UniqueRowsReader.class);
    private final Map<Integer, Long> hashPointerMap = new HashMap<>();
    private Set<Integer> duplicates = new HashSet<>();

    public void read(final String path) {
        logger.info("Start creating hashtable for " + path);
        try (final RandomAccessFile reader = new RandomAccessFile(path, "r")) {
            String line;
            long pointer = reader.getFilePointer();

            while ((line = reader.readLine()) != null) {
                final int hash = line.hashCode();

                if (duplicates.contains(hash)) {
                    continue;
                }

                if (hashPointerMap.get(hash) != null) {
                    duplicates.add(hash);
                    hashPointerMap.remove(hash);
                    continue;
                }

                hashPointerMap.put(hash, pointer);
                pointer = reader.getFilePointer();
            }
            logger.info("Hashtable populated with " + hashPointerMap.size() + " unique rows. " + duplicates.size() + " non-unique rows are ignored.");
            duplicates = null;
        }
        catch (final IOException e){
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Map<Integer, Long> getHashPointerMap() {
        return hashPointerMap;
    }
}
