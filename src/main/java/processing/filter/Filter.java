package processing.filter;

import processing.domain.Person;

import java.util.Collection;

public interface Filter {

    Collection<Person> filter(Collection<Person> persons);

}
