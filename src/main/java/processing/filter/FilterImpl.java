package processing.filter;

import processing.domain.Person;

import java.util.Collection;
import java.util.Comparator;

import static java.util.stream.Collectors.toList;

public final class FilterImpl implements Filter{

    private final Comparator<Person> comparator;

    public FilterImpl(final Comparator<Person> comparator) {
        this.comparator = comparator;
    }

    @Override
    public Collection<Person> filter(final Collection<Person> persons) {
        return persons.stream().sorted(comparator).collect(toList());
    }
}
